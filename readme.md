# yarnberry

Terminal steps to reproduce

- `yarn set version berry`


Seems to have an error when trying to install.
 can use :- after variable in .yarnrc.yml so you don't get an error.

Found issue. It is cap sensitive for the scope.

When using dotenv and setting up the plugin for Yarn 2, it will not work because plugins are loaded before Yarn starts. We need to bundle our plugin dep (dotenv). We can use the bundler for yarn
- yarn add -D @yarnpkg/builder typescript

Note: yarn builder docs suck. This is the correct command to bundle
- yarn run builder new plugin plugin-dotenv

It's neccesary to add a yarn.lock file or errors will occur and nothing can be done.

Useful Links:
https://www.npmjs.com/package/@yarnpkg/builder
https://yarnpkg.com/advanced/plugin-tutorial#hook-setupScriptEnvironment
https://github.com/yarnpkg/berry/blob/master/packages/yarnpkg-builder/README.md
https://github.com/Ronbb/yarn-plugin-env
https://yarnpkg.com/cli/plugin/import
